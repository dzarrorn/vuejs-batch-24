//Nomor1
console.log('-----------Nomor 1------------')
const luasPersegiPanjang = (panjang, lebar) => {
    const luas = panjang * lebar
    return luas
}
const kelilingPersegiPanjang = (panjang, lebar) => {
    const keliling = 2 * (panjang + lebar)
    return keliling
}
console.log(kelilingPersegiPanjang(2, 4))
console.log(luasPersegiPanjang(2, 4))

//Nomor2 
console.log('-----------Nomor 2------------')
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => console.log(`${firstName} ${lastName}`)
    }
}
newFunction("William", "Imoh").fullName()

//Nomor3
console.log('-----------Nomor 3------------')
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)

//Nomor4 
console.log('-----------Nomor 4------------')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

//Nomor5
console.log('-----------Nomor 5------------')
const planet = "earth"
const view = "glass"
let after = `Lorem ${view} dolor sit amet consectetur adipiscing elit, ${planet}`
console.log(after)