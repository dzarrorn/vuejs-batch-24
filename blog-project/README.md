## Link Gdrive
```
https://drive.google.com/drive/folders/1WzgQT3DFSbz4HRU8JITnlYJEzaNCFWVK?usp=sharing
```
## Link Project
```
https://optimistic-thompson-84672c.netlify.app/
```


# blog-project

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


