var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

const time = 10000

const execute = async() => {
    const sisawaktu0 = await readBooksPromise(time, books[0])
    const sisawaktu1 = await readBooksPromise(sisawaktu0, books[1])
    const sisawaktu2 = await readBooksPromise(sisawaktu1, books[2])
    const sisawaktu3 = await readBooksPromise(sisawaktu2, books[3])
        // const sisawaktu0 = await readBooksPromise(time, books[0])
        // console.log(sisawaktu0)
        // const sisawaktu1 = await readBooksPromise(sisawaktu0.data.sisaWaktu, books[1])
        // const sisawaktu2 = await readBooksPromise(sisawaktu1, books[2])
        // const sisawaktu3 = await readBooksPromise(sisawaktu2, books[3])
    return console.log('selesai')
}
execute()